**Choix des technologies**

Albums App est une application android native qui permet d'afficher une liste d'albums suivant : https://static.leboncoin.fr/img/shared/technical-test.json

- Kotlin :

Comme c'est une application native mon choix du langage s'est porté directement sur Kotlin, un langage bien plus concis que Java dans de nombreux cas, résolvant les mêmes problèmes avec moins de lignes de code. Cela améliore la maintenabilité et la lisibilité du code, et nous permet une forte stabilisation de l'application. 


- Architecture MVVM :

Pour séparer les responsabilités dans l'application, j'ai choisi d'utiliser l'architecture MVVM (Model-View-ViewModel), cela me permet une plus grande modularité du code: les modules peuvent être développés de manières plus indépendante, il est plus facile de les tester, d'en assurer la maintenance et de les mettre à jour.


- Retrofit :

Pour manipuler l'API, Retrofit me semblait plus logique, c'est une puissante bibliothèque facilite la consommation de données JSON
qui sont ensuite analysées en objets Java simples (POJO).

- Mockito et JUnit:

J'avais utiliser Mockito et JUnit pour realiser les tests unitaires. Les deux bibliothèques s’utilisent conjointement, JUnit permet de créer les tests unitaires du code applicatif. Mockito quant à lui va nous permettre de simuler les comportements des dépendances afin d’isoler nos tests.
