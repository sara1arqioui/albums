package fr.mastergime.arqioui.albums.model.viewmodels

import java.io.BufferedReader
import java.io.File

class MockResponseFileReader(path: String) {

    val inputString: String

    init {

        val bufferedReader: BufferedReader = File(path).bufferedReader()
        inputString = bufferedReader.use { it.readText() }

    }
}
