package fr.mastergime.arqioui.albums.model.viewmodels

import fr.mastergime.arqioui.albums.repository.RequestRepository
import junit.framework.Assert.*
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.json.JSONObject
import org.junit.*
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit


@RunWith(MockitoJUnitRunner::class)
class AlbumsViewModelTest{

    @get:Rule
    private lateinit var albumViewModel : AlbumsViewModel
    private lateinit var mockWebServer: MockWebServer

    var mRepo = RequestRepository()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        albumViewModel = AlbumsViewModel(RequestRepository())
        mockWebServer = MockWebServer()
        mockWebServer.start()
    }


    @Test
    fun `read sample success json file`(){
        val reader = MockResponseFileReader("C:\\Users\\sara-\\AndroidStudioProjects\\Albums\\app\\src\\main\\assets\\success_response.json")
        assertNotNull(reader.inputString)
    }

    @Test
    fun `get albums and check response Code 200 returned`() = runBlocking {
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(MockResponseFileReader("C:\\Users\\sara-\\AndroidStudioProjects\\Albums\\app\\src\\main\\assets\\success_response.json").inputString)
        mockWebServer.enqueue(response)

        // Act
        val actualResponse = mRepo.getAlbums()

        // Assert
        assertEquals(response.toString().contains("200"), actualResponse.code().toString().contains("200"))
    }

    @Test
    fun `test failed response after 5 seconds`() {
        mockWebServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return MockResponse().throttleBody(1024, 5, TimeUnit.SECONDS)
            }
        }
    }


    @Test
    fun `test first title`() = runBlocking{
        // Assign
        val response = MockResponse()
            .setResponseCode(HttpURLConnection.HTTP_OK)
            .setBody(MockResponseFileReader("C:\\Users\\sara-\\AndroidStudioProjects\\Albums\\app\\src\\main\\assets\\success_response.json").inputString)
        mockWebServer.enqueue(response)
        val mockResponse = response.getBody()?.readUtf8()

        // Act
        val actualResponse = mRepo.getAlbums()

        // Assert
        assertEquals(mockResponse?.let { `parse mocked JSON response`(it) },
            actualResponse.body()?.get(0)?.title
        )
    }


    private fun `parse mocked JSON response`(mockResponse: String): String {
        val reader = JSONObject(mockResponse.replace("[", "").replace("]", ""))
        return reader.getString("title")
    }

    @Test
    fun `album list is not empty`(){
        val actualResponse = albumViewModel.albumsResponse
        actualResponse.value?.body()?.isEmpty()?.let { assertFalse(it) }
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

}