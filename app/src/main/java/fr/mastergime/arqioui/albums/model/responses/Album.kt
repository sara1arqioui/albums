package fr.mastergime.arqioui.albums.model.responses

data class Album (
    val albumId : Int,
    val id : Int,
    val title : String,
    val url : String,
    val thumbnailUrl : String
)