package fr.mastergime.arqioui.albums.repository

import fr.mastergime.arqioui.albums.api.ApiService
import fr.mastergime.arqioui.albums.model.responses.Album
import retrofit2.Response

class RequestRepository {

    suspend fun getAlbums(): Response<List<Album>>{
        return ApiService.API.getAlbums()
    }
}