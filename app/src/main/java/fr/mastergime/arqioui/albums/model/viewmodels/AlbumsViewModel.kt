package fr.mastergime.arqioui.albums.model.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.mastergime.arqioui.albums.model.responses.Album
import fr.mastergime.arqioui.albums.repository.RequestRepository
import kotlinx.coroutines.launch
import retrofit2.Response

class AlbumsViewModel(private val repository: RequestRepository) : ViewModel() {

    var albumsResponse: MutableLiveData<Response<List<Album>>> = MutableLiveData()

    fun getAlbums() {
        viewModelScope.launch() {
            val response = repository.getAlbums()
            albumsResponse.value = response
        }

    }
}