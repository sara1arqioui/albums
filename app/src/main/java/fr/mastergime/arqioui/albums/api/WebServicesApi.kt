package fr.mastergime.arqioui.albums.api

import fr.mastergime.arqioui.albums.model.responses.Album
import retrofit2.Response
import retrofit2.http.GET

interface WebServicesApi {

    @GET("img/shared/technical-test.json")
    suspend fun getAlbums(): Response<List<Album>>
    
}