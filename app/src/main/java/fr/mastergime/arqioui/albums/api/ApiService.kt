package fr.mastergime.arqioui.albums.api

import fr.mastergime.arqioui.albums.api.URLs.Companion.ALBUMS_URL
import fr.mastergime.arqioui.albums.exceptions.NetworkConnectionInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiService {

    private val retrofit by lazy{
        Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(ALBUMS_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    val API : WebServicesApi by lazy{
        retrofit.create(WebServicesApi::class.java)
    }

    private val networkInterceptor = NetworkConnectionInterceptor()

    private val okHttpClient =
            OkHttpClient.Builder().addInterceptor(networkInterceptor).build()

}

