package fr.mastergime.arqioui.albums.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import fr.mastergime.arqioui.albums.model.viewmodels.AlbumsViewModel
import fr.mastergime.arqioui.albums.repository.RequestRepository

class AlbumsViewModelFactory(
    private val repository: RequestRepository
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AlbumsViewModel(repository) as T
    }
}