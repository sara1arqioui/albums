package fr.mastergime.arqioui.albums.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import fr.mastergime.arqioui.albums.databinding.AlbumsFragmentLayoutBinding
import fr.mastergime.arqioui.albums.factory.AlbumsViewModelFactory
import fr.mastergime.arqioui.albums.model.viewmodels.AlbumsViewModel
import fr.mastergime.arqioui.albums.repository.RequestRepository
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import fr.mastergime.arqioui.albums.adapter.AlbumsAdapter
import kotlinx.android.synthetic.main.album_item_layout.*
import kotlinx.coroutines.handleCoroutineException
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException

class AlbumsFragment : Fragment() {
    private lateinit var _albumsBinding: AlbumsFragmentLayoutBinding
    private val albumsAdapter by lazy { AlbumsAdapter() }

    private val albumsViewModel: AlbumsViewModel by viewModels(factoryProducer = {
        AlbumsViewModelFactory(
                RequestRepository()
        )
    })

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.title = "Albums"

        _albumsBinding = AlbumsFragmentLayoutBinding.inflate(inflater)
        return _albumsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerview()
        albumsViewModel.getAlbums()

        albumsViewModel.albumsResponse.observe(viewLifecycleOwner, Observer { response ->

            if (response.isSuccessful) {
                response.body()?.let { albumsAdapter.setData(it) }
            } else {
                Toast.makeText(context,response.message(), Toast.LENGTH_LONG).show()
            }

        })

    }

    private fun setupRecyclerview() {
        val itemDecor = DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        _albumsBinding.recyclerViewAlbums.adapter = albumsAdapter
        _albumsBinding.recyclerViewAlbums.layoutManager = LinearLayoutManager(context)
        _albumsBinding.recyclerViewAlbums.addItemDecoration(itemDecor)

    }

}