package fr.mastergime.arqioui.albums.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import fr.mastergime.arqioui.albums.R
import fr.mastergime.arqioui.albums.model.responses.Album
import kotlinx.android.synthetic.main.album_item_layout.view.*
import java.lang.Exception

class AlbumsAdapter: RecyclerView.Adapter<AlbumsAdapter.AlbumsViewHolder>() {

    private var albumsList = emptyList<Album>()
    var url: String = ""

    inner class AlbumsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumsViewHolder {
        return AlbumsViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.album_item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return albumsList.size
    }

    override fun onBindViewHolder(holder: AlbumsViewHolder, position: Int) {
        holder.itemView.albumId_text.text = albumsList[position].albumId.toString()
        holder.itemView.id_text.text = albumsList[position].id.toString()
        holder.itemView.title_text.text = albumsList[position].title
        holder.itemView.url_text.text = albumsList[position].url

        url = albumsList[position].thumbnailUrl
        Picasso.get()
            .load("$url").into(holder.itemView.imagethumbnailURL, object : Callback {
                override fun onSuccess() {
                    holder.itemView.progressbar.visibility = View.GONE
                }

                override fun onError(e: Exception?) {
                    TODO("Not yet implemented")
                }
            })
    }

    fun setData(newList: List<Album>) {
        albumsList = newList
        notifyDataSetChanged()
    }

}